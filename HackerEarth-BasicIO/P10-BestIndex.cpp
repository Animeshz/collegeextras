#include <iostream>

using namespace std;

int main(){
    int n;
    cin >> n;

    int *a = new int[n];
    for (int i = 0; i < n; i++) cin >> a[i];

    int best_idx = n-1, k = 2;
    long sum = a[n-1];  // cornor case: long over int
    long maxsum = sum;

    for (int i=n-2; i >= 0; i--) {
        sum = sum + a[i] - a[i+k*(k-1)/2];

        // if n-i is in form of k*(k+1)/2, then add all last elements
        if (n-i == k*(k+1)/2) {
            for (int j = n-1; j >= n-k; j--) sum += a[j];
            k += 1;
        }
        maxsum = max(maxsum, sum);
    }

    cout << maxsum << endl;

    delete a;
    return 0;
}
