#include<iostream>

using namespace std;

int main(){
    int n;
    cin >> n;

    int *A = new int[n];

    for (int i = 0; i < n; i++) cin >> A[i];

    if (A[n-1] % 10 == 0) {
        cout << "Yes" << endl;
    } else {
        cout << "No" << endl;
    }

    delete A;
    return 0;
}
