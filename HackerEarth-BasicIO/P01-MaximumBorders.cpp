#include <iostream>

using namespace std;

int main(){
    int t;
    cin >> t;

    while (t--) {
        int n, m;
        cin >> n >> m;
        int *starts = new int[n];
        int *ends = new int[n];
        for (int i = 0; i < n; i++)
            starts[i] = -1;
        for (int i = 0; i < n; i++)
            ends[i] = -1;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++) {
                char ch;
                cin >> ch;
                if (starts[i] == -1 && ch == '#') {
                    starts[i] = j;
                } else if (starts[i] != -1 && ends[i] == -1 && ch == '.') {
                    ends[i] = j - 1;
                }
            }

//        for (int i = 0; i < n; i++)
//            cout << "s,e: " << starts[i] << ',' << ends[i] << endl;


        int maximum = max(ends[0] - starts[0] + 1, ends[n-1] - starts[n-1] + 1);
        int vert_l = 1;
        int vert_r = 1;

        for (int i = 1; i < n; i++) {
            if (starts[i-1] == -1 && starts[i] != -1) {
                maximum = max(maximum, ends[i] - starts[i] + 1);
                continue;
            }
            if (starts[i] == -1 && starts[i-1] != -1) {
                maximum = max(maximum, ends[i-1] - starts[i-1] + 1);
                continue;
            }

            if (starts[i-1] == starts[i]) {
                vert_l += 1;
            } else {
                maximum = max(maximum, vert_l);
                vert_l = 1;
            }

            if (ends[i-1] == ends[i]) {
                vert_r += 1;
            } else {
                maximum = max(maximum, vert_r);
                vert_r = 1;
            }

            if (starts[i] > ends[i-1] || starts[i-1] > ends[i]) {
                maximum = max(maximum, ends[i] - starts[i] + 1);
                maximum = max(maximum, ends[i-1] - starts[i-1] + 1);
            } else {
                maximum = max(maximum, abs(starts[i] - starts[i-1]));
                maximum = max(maximum, abs(ends[i] - ends[i-1]));
            }
        }

        cout << maximum << endl;

        delete starts;
        delete ends;
    }

    return 0;
}
