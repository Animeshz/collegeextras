#include <iostream>

using namespace std;

int main(){
    int t;
    cin >> t;

    while (t--) {
        int g, p, n;
        cin >> g >> p >> n;

        int *a = new int[n];
        int *b = new int[n];
        for (int i = 0; i < n; i++) {
            cin >> a[i] >> b[i];
        }

        int suma = 0, sumb = 0;
        for (int i = 0; i < n; i++) {
            suma += a[i];
            sumb += b[i];
        }

        int minimum = max(suma, sumb) * min(g, p) + min(suma, sumb) * max(g, p);
        cout << minimum << endl;

        delete a;
        delete b;
    }

    return 0;
}
