#include <iostream>
#include <cstring>

using namespace std;

int main(){
    int sticks[10] = {6,2,5,5,4,5,6,3,7,6};

    int t;
    cin >> t;
    cin.ignore();  // ignore-case: newline at first line

    while (t--) {
        string in;
        getline(cin, in);

        // 1 & 7 for max digits
        int total_sticks = 0;
        for (int i = 0; i < in.length(); i++) {
            int idx = in[i] - '0';
            total_sticks += sticks[idx];
        }

        string out = "";
        if (total_sticks % 2 == 0) {
            for (int i = 0; i < total_sticks/2; i++) out.append("1");
        } else {
            out.append("7");
            for (int i = 0; i < total_sticks/2 - 1; i++) out.append("1");
        }

        cout << out << endl;
    }

    return 0;
}
