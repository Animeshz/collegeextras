#include <iostream>

using namespace std;

int main(){
    int n, k;
    cin >> n >> k;

    long *a = new long[n];
    long *t = new long[n]{0};

    for (int i = 0; i < n; i++) cin >> a[i];


    // Flawed approach: O(n*k)
    // for (int i = 0; i < n; i++) {
    //     for (int j = max(i-k, 0); j <= min(i+k, n-1) && a[i] + t[i] > 0; j++) {
    //         if (a[j] + t[j] < 0) {
    //             int delta = min(abs(a[j] + t[j]), a[i] + t[i]);  // delta = min(required at j, available at i)
    //             t[j] += delta;
    //             t[i] -= delta;
    //         }
    //     }
    // }


    // include next, exclude previous O(max(n, k)) method
    long available_sum = 0;
    long loans = 0;
    for (int i = 0; i < k; i++) {
        if (a[i] > 0) available_sum += a[i];
    }

    for (int i = 0; i < n; i++) {
        if (i-k-1 >= 0 && a[i-k-1] > 0) {
            available_sum -= a[i-k-1];
            t[i-k-1] = -1 * min(loans, a[i-k-1]);
            loans += t[i-k-1];
        }

        if (i+k < n && a[i+k] > 0) {
            available_sum += a[i+k];
        }

        if (a[i] < 0) {
            t[i] = min(abs(a[i]), max(0l, available_sum - loans));
            loans += t[i];
        }
        // cout << "i: " << i << " available_sum: " << available_sum << " loans: " << loans << endl;
    }

    for (int i = 0; i <= k && loans > 0; i++) {
        if (a[n-i-1] > 0) {
             t[n-i-1] = -1 * min(loans, a[n-i-1]);
             loans -= t[n-i-1];
        }
    }

    // cout << "k: " << k << endl;
    // cout << "a: ";
    // for (int i = 0; i < n; i++) cout << a[i] << ' ';
    // cout << endl;
    // cout<< "t: ";
    // for (int i = 0; i < n; i++) cout << t[i] << ' ';
    // cout << endl;

    long sum = 0;
    for (int i = 0; i < n; i++) {
        sum += abs(a[i] + t[i]);
    }

    cout << sum << endl;

    delete a;
    delete t;
    return 0;
}
