#include <iostream>

using namespace std;

int main(){
    int n;
    cin >> n;

    int *a = new int[n];
    for (int i = 0; i < n; i++) cin >> a[i];

    int *dig = new int[n];
    for (int i = 0; i < n/2; i++) {
        dig[i] = a[i] % 10;  // cornor-case
        while (a[i] /= 10) dig[i] = a[i] % 10;
    }
    for (int i = n/2; i < n; i++) {
        dig[i] = a[i] % 10;
    }

    long int even = 0, odd = 0;
    for (int i = 0; i < n; i++) {
        if (i % 2 == 0) even += dig[i];
        else odd += dig[i];
    }

    if ((even - odd) % 11 == 0) {
        cout << "OUI" << endl;
    } else {
        cout << "NON" << endl;
    }

    delete a;
    return 0;
}
