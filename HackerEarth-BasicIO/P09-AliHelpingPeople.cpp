#include <iostream>
#include <cstring>

using namespace std;

int main(){
    // format: DDXDDD-DD;
    string in;
    getline(cin, in);

    bool is_vowel = in[2] == 'A' || in[2] == 'E' || in[2] == 'I' || in[2] == 'O' || in[2] == 'U' || in[2] == 'Y';
    if ((in[0] + in[1]) % 2 == 0 && !is_vowel && (in[3] + in[4]) % 2 == 0 && (in[4] + in[5]) % 2 == 0 && (in[7] + in[8]) % 2 == 0) {
        cout << "valid" << endl;
    } else {
        cout << "invalid" << endl;
    }


    return 0;
}
