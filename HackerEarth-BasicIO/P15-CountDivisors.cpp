#include <iostream>

using namespace std;

int main(){
    int l, r, k;
    cin >> l >> r >> k;

    int count = 0;
    for (int i = l; i <= r; i++) {
        count += i % k == 0;
    }

    cout << count << endl;

    return 0;
}
