#include <iostream>
#include <cstring>

using namespace std;

int main(){
    string in;
    getline(cin, in);

    int n = in.length();
    for (int i = 0; i < n/2; i++) {
        if (in[i] != in[n-1-i]) {
            cout << "NO" << endl;
            return 0;
        }
    }
    cout << "YES" << endl;

    return 0;
}
