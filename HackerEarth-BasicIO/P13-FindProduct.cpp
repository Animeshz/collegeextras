#include <iostream>

using namespace std;

int main(){
    int n, k;
    cin >> n;

    long prod = 1;
    for (int i = 0; i < n; i++) {
        cin >> k;
        prod *= k;
        prod %= 1000000007;
    }
    cout << prod << endl;

    return 0;
}
