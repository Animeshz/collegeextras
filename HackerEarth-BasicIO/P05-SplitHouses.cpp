#include <iostream>

using namespace std;

int main(){
    int n;
    cin >> n;

    char *grids = new char[n];
    cin >> grids[0];
    for (int i = 1; i < n; i++) {
        cin >> grids[i];
        if (grids[i-1] == 'H' && grids[i] == 'H') {
            cout << "NO" << endl;
            return 0;
        }
    }

    cout << "YES" << endl;
    for (int i = 0; i < n; i++) {
        if (grids[i] == '.') cout << "B";
        else cout << grids[i];
    }
    cout << endl;

    return 0;
}
