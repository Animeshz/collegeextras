#include <iostream>

using namespace std;

int main(){
    string in;
    getline(cin, in);

    for (int i=0; i < in.length(); i++) {
        if (in[i] >= 'a' && in[i] <= 'z') {
            cout << (char) (in[i] + 'A' - 'a');
        } else if (in[i] >= 'A' && in[i] <= 'Z') {  // cornor case for space / symbols
            cout << (char) (in[i] + 'a' - 'A');
        } else {
            cout << (char) in[i];
        }
    }
    cout << endl;

    return 0;
}
