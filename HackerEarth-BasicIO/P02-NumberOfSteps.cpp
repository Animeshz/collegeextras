#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main(){
    int n;
    cin >> n;

    int *a = new int[n];
    int *b = new int[n];

    for (int i = 0; i < n; i++) cin >> a[i];
    for (int i = 0; i < n; i++) cin >> b[i];

    vector<vector<int>> possibilities(n);
    for (int i = 0; i < n; i++) possibilities[i] = vector<int>();

    for (int i = 0; i < n; i++) {
        // cout << i << ' ' << a[i] << ' ' << b[i] << ' ' << (double) a[i] / b[i] << ceil((double) a[i] / b[i]) << endl;

        if (b[i] == 0) {  // cornor case
            possibilities[i].push_back(a[i]);
            continue;
        }

        for (int j = 0; j < ceil((double) a[i]/b[i]) + 1; j++) {
            possibilities[i].push_back(a[i] - j * b[i]);
        }
    }

    // cout << "possibilities" << endl;
    // for (int i = 0; i < n; i++) {
    //     for (int j = 0; j < possibilities[i].size(); j++) {
    //         cout << possibilities[i][j] << ' ';
    //     }
    //     cout << endl;
    // }

    // min dhundho, niche lekr jao us tk ya uske just niche, repeat from index 0 if not.
    int minimum = possibilities[0][0];
    for (int i = 1; i < n; i++) {
//        cout << i << ' ' << n << "size: " << possibilities[i].size() << endl;
        minimum = min(minimum, possibilities[i][0]);
    }

LOOP:
    for (int i = 0; i < n; i++) {
        int j = 0;
        while (possibilities[i][j] > minimum) {
            if (++j == possibilities[i].size()) {
                // cout << "i, j: " << i << ' ' << j << endl;
                cout << -1 << endl;
                return 0;
            }
        }
        if (possibilities[i][j] != minimum) {
            minimum = possibilities[i][j];
            goto LOOP;
        }
    }

    // we got the common minimum
    // cout << "minimum is: " << minimum << endl;

    int total_steps = 0;
    for (int i = 0; i < n; i++) {
        if (b[i] == 0) continue;  // cornor case
        total_steps += (a[i] - minimum) / b[i];
    }

    cout << total_steps << endl;

    delete a;
    delete b;

    return 0;
}
