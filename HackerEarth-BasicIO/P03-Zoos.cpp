#include <iostream>
#include <string>

using namespace std;

int main(){
    string in;
    getline(cin, in);

    int z_ct = 0;
    while (in[++z_ct] == 'z');

    if (in.length() - z_ct == z_ct * 2) {
        cout << "Yes" << endl;
    } else {
        cout << "No" << endl;
    }

    return 0;
}
