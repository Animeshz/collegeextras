#include <iostream>

using namespace std;

int main(){
    int n;
    cin >> n;

    int fac = n;
    while (--n) fac *= n;

    cout << fac << endl;

    return 0;
}
