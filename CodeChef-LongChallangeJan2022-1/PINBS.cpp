#include <bits/stdc++.h>

using namespace std;

// ref: https://gist.github.com/rongjiecomputer/d52f34d27a21b8c9c9e82ca85b806640
template <size_t N>
struct PrimeTable {
  constexpr PrimeTable() : sieve() {
    sieve[0] = sieve[1] = false;
    for (size_t i = 2; i < N; i++) sieve[i] = true;
    for (size_t i = 2; i < N; i++) {
      if (sieve[i])
        for (size_t j = i*i; j < N; j += i) sieve[j] = false;
    }
  }
  bool sieve[N];
};

string binary(int n) {
    string s;
    while (n) {
        s.push_back(n % 2 + '0');
        n /= 2;
    }
    reverse(s.begin(), s.end());
    return s;
}


int main(){
    constexpr auto table = PrimeTable<1000>();

    int t;
    cin >> t;

    while (t--) {
        string s;
        cin >> s;

        for (int i = 0; i < 1000; i++) {
            if (table.sieve[i] && s.find(binary(i)) != string::npos) {
                cout << "Yes" << endl;
                goto outerend;
            }
        }
        cout << "No" << endl;

outerend:;
    }
    return 0;
}
