# [CodeChef Jan 2022 (1) Div C solutions](https://codechef.com/JAN221C)

| Problem    | Status             |
|------------|--------------------|
| KEPLERSLAW | Accepted           |
| COVSPRD    | Accepted           |
| PINBS      | Accepted           |
| XORED      | Accepted           |
| RIFFLES    | Accepted           |
| MASTER     | TLE & Partially WA |

