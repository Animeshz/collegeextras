#include <iostream>
//#include <bits/stdc++.h>

using namespace std;

//// function to find and print duplicates
//void printDuplicates(int arr[], int n) {
//    // unordered_map to store frequencies
//    unordered_map<int, int> freq;
//    for (int i=0; i<n; i++)
//        freq[arr[i]]++;
//
//    bool dup = false;
//    unordered_map<int, int>:: iterator itr;
//    for (itr=freq.begin(); itr!=freq.end(); itr++) {
//        // if frequency is more than 1
//        // print the element
//        if (itr->second > 1) {
//            cout << itr->first << " ";
//            dup = true;
//        }
//    }
//
//    // no duplicates present
//    if (dup == true) {
//        cout << "n: " << n << endl;
//        //throw runtime_error("dupe");
//    }
//}

//inline
//void swapbit(unsigned int *A, unsigned int *B, unsigned int mask)
//{
//    unsigned char tmpA = *A, tmpB = *B;
//
//    unsigned char bitdiff = tmpA ^ tmpB;
//    bitdiff &= mask;
//    *A = tmpA ^ bitdiff;
//    *B = tmpB ^ bitdiff;
//}

int leftmost_bit(int n)
{
    if (n == 0)
        return 0;

    int msb = 0;
    while (n /= 2) msb++;
    return 1 << msb;
}

int main(){
    int t;
    cin >> t;

    while (t--) {
        int n, x;
        cin >> n >> x;

        if (n == 1) {
            cout << x << endl;
            continue;
        }

        int xored = 0;
        int *arr = new int[n];
        for (int i = 1; i < n-1; i++) {
            arr[i-1] = i;
            xored ^= i;
        }

        // Treat last two numbers, or second last n last
        // Flaw: do something better, make n < 'b' < 5 * 1e5
        //int *a, *b;
        //if (arr[n-2] != arr[n-1]) {
        //    a = arr + n-2;
        //    b = arr + n-1;
        //} else {
        //    a = arr + n-3;
        //    b = arr + n-1;
        //}
        //int common_off_bits = ~(*a | *b) & 0x3FFFF /* 18 bits */;
        //*a |= common_off_bits;
        //*b |= common_off_bits;

        int required_result;
        while ((required_result = (x ^ xored)) == 0) {
            xored ^= arr[n-3];
            arr[n-3]++;
            xored ^= arr[n-3];
        }

        unsigned int l_bit = leftmost_bit(required_result);
        //cout << "required_result: " << required_result << endl;
        //cout << "l_bit: " << l_bit << endl;

        // Take care of 19 bits
        unsigned int a = 0, b = 0;

        for (int i = 18; i >= 0; i--) {
            a |= (required_result & (1 << i)) ? 0 : (1 << i);
            b |= (1 << i);
        }

        a |= l_bit;
        b &= ~l_bit;

        if (a > 5 * 1e5 || b > 5 * 1e5) {
            if (required_result & (1 << 18)) {
                // if just unsetting b: wrong ans as xor won't happen
                arr[0] |= (1 << 18) & a;
                arr[1] |= (1 << 18) & b;
            }
            a &= ~(1 << 18);
            b &= ~(1 << 18);
        }

        //if (a < n-1 || b < n) {
        //    throw runtime_error("cond");  // just a way to check if runtime error occur
        //}
        if (a == b) {  // cornor case
            //throw runtime_error("ab");  // checked the cornor case
            // arr[0] a b, have a cleanup

            // Flaw: 3 bits are not always 0
            // find 3 bits commonly zero in 17 bits 1 to (1 << 17)
            //int idx[3] = {0};
            //int l = 0;
            //for (int i = 1; i <= 17 && l < 3; i++) {
            //    if ((a & (1 << i)) == 0) idx[l++] = i;
            //}
            //if (l != 3) throw runtime_error("3 not present");
            //
            //a |= mask1;
            //b |= mask2;
            //arr[0] |= mask3;

            // Flaw fix
            int mask1 = (1 << 1) | (1 << 2);
            int mask2 = (1 << 1) | (1 << 3);
            int mask3 = (1 << 2) | (1 << 3);

            a ^= mask1;
            b ^= mask2;
            arr[0] ^= mask3;
        }

        arr[n-2] = a;
        arr[n-1] = b;

        for (int i = 0; i < n; i++)
            cout << arr[i] << ' ';
        cout << endl;

        //printDuplicates(arr, n);

        //int testxor = 0;
        //for (int i = 0; i < n; i++)
        //    testxor ^= arr[i];
        //if (testxor != x) throw runtime_error("xor");
    }
    return 0;
}
