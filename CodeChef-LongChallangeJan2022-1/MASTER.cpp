#include <bits/stdc++.h>

using namespace std;

//unordered_map<int, int> elements_freq(int *arr, int start, int end) {
//    unordered_map<int, int> freq;
//    for (int i=start; i<=end; i++)
//        freq[arr[i]]++;
//
//    return freq;
//}

int main() {
    int n, q;
    cin >> n >> q;

    int *arr = new int[n];
    for (int i = 0; i < n; i++) cin >> arr[i];

    // O(max(max(k)^2, q)) ~ 4 ^ 10^10 instructions, taking N upper limit
    //unordered_map<int, int> freq;
    //int *f = new int[n];

    //// Flaw: Eager calculation: here & at query1, o(n^2)
    ////for (int k = 1; k < n; k++) {
    ////    for (int i = 0; i <= k; i++) freq[arr[k]]++;

    ////    f[k] = f[k-1];
    ////    for (int i = 0; i <= k; i++) {
    ////        f[k] += freq.size();
    ////        if (--freq[arr[i]] == 0) freq.erase(arr[i]);
    ////    }
    ////}

    //int calculated_freq = 0;

    //while (q--) {
    //    int type;
    //    cin >> type;

    //    if (type == 2) {
    //        int k;
    //        cin >> k;

    //        if (k > calculated_freq) {
    //            // O(max(k)^2)
    //            for (int i = calculated_freq; i < k; i++) {
    //                f[i] = i == 0 ? 0 : f[i-1];
    //                for (int j = i; j >= 0; j--) {
    //                    freq[arr[j]]++;
    //                    f[i] += freq.size();
    //                }
    //                freq.clear();
    //            }
    //            calculated_freq = k;
    //        }

    //        cout << f[k-1] << endl;
    //    } else {
    //        int x, y;
    //        cin >> x >> y;

    //        //// But we need to count the frequency and not the delta :(
    //        //int delta = y - arr[x-1];
    //        arr[x-1] = y;
    //        //for (int i = x-1, old_fi1 = f[i-1]; i < calculated_freq; i++) {
    //        //    f[i] = f[i] - old_fi1 + f[i-1];
    //        //    old_fi1 = f[i];
    //        //    f[i] += x * delta;
    //        //}

    //        // Temporary fix (needs recalculation starting from index x-1)
    //        calculated_freq = x-1;
    //    }
    //}

    //delete [] arr;
    //delete [] f;


    // O(q*k) still ~ 4 ^ 10^10 instructions, taking N upper limit
    //int *f = new int[n];

    //unordered_map<int, int> last_idx;
    //// Have to do this till k only
    ////for (int i = 0; i < n; i++) last_idx[arr[i]] = i;

    //while (q--) {
    //    int type;
    //    cin >> type;

    //    if (type == 2) {
    //        int k;
    //        cin >> k;

    //        for (int i = 0; i < k; i++) last_idx[arr[i]] = i;
    //        int sum = k * (k+1) * (k+2) / 6;  // max sum possible, if all elements are distinct

    //        for (int i = 0; i < k; i++) {
    //            if (last_idx[arr[i]] != i) sum -= (i+1) * (k - last_idx[arr[i]]);
    //        }
    //        last_idx.clear();

    //        cout << sum << endl;
    //    } else {
    //        int x, y;
    //        cin >> x >> y;
    //    }
    //}

    //delete [] f;

    map<int, vector<int>> occurrences;
    long *f = new long[n];

    int calculated_freq = 0;

    while (q--) {
        int type;
        cin >> type;

        if (type == 2) {
            int k;
            cin >> k;

            if (k > calculated_freq) {
                for (int i = calculated_freq; i < k; i++) {
                    f[i] = (i == 0 ? 0 : f[i-1]) + (i+1) * (i+2) / 2;
                    if (occurrences.find(arr[i]) == occurrences.end()) {
                        occurrences[arr[i]] = vector<int>(1, i);
                    } else {
                        occurrences[arr[i]].push_back(i);
                        int last_idx = occurrences[arr[i]].size() - 1;
                        for (int j = 0; j < last_idx; j++) {
                            f[i] -= (occurrences[arr[i]][j]+1) /* * (i+1 - occurrences[arr[i]][last_idx]) == 1 */;
                        }
                    }
                }
                calculated_freq = k;
            }

            cout << f[k-1] << endl;
        } else {
            int x, y;
            cin >> x >> y;

            // Temporary fix (needs recalculation starting from index x-1)
            for (int i = x-1; i < calculated_freq; i++) {
                remove_if(begin(occurrences[arr[x-1]]), end(occurrences[arr[x-1]]),
                    [x](int& v) { return v >= x-1; });
            }
            for (auto it = occurrences.begin(); it != occurrences.end();) {
                (it->second.size() == 0) ? occurrences.erase(it++) : (++it);
            }
            arr[x-1] = y;
            calculated_freq = x-1;
        }
    }

    delete [] arr;
    delete [] f;

    return 0;
}
