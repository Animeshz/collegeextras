#include <iostream>

using namespace std;

//int equals(int *arr1, int *arr2, int n) {
//    for (int i = 0; i < n; i++)
//        if (arr1[i] != arr2[i]) return false;
//    return true;
//}

int main(){
    int t;
    cin >> t;

    while (t--) {
        int n, k;
        cin >> n >> k;

        // Flawed approach O((k%n) * n) basically O(n*n), also k %= n-2 is not always valid.
        //if (n > 3)      // cornor case: n == 2 or 3
        //    k %= n-2;
        //
        //k += 1;
        //int **arr = new int*[k];
        //for (int i = 0; i < k; i++) arr[i] = new int[n];
        //for (int i = 0; i < n; i++) arr[0][i] = i+1;
        //for (int i = 1; i < k; i++) {
        //    //for (int l = 0; l < n; l++) cout << arr[i-1][l] << ' ';
        //    //cout << endl;
        //    for (int j = 0; j < (n+1)/2; j++) arr[i][j] = arr[i-1][2*j];
        //    for (int j = (n+1)/2; j < n; j++) arr[i][j] = arr[i-1][2*(j-(n+1)/2)+1];
        //    //if (equals(arr[0], arr[i], n)) {
        //    //    cout << "n,k: " << n << ',' << k << " repeat start at i: " << i << endl;
        //    //    goto end_of_while;
        //    //}
        //}
        ////continue;
        //for (int l = 0; l < n; l++) cout << arr[k-1][l] << ' ';
        //cout << endl;
        //for (int i = 0; i < k; i++) delete [] arr[i];
        //delete [] arr;

        // Flawed approach: double overflow 0 < 'k' < n (=3*10^5)
        //int l = 1, diff = (int) pow(2, k);
        //for (int i = 0; i < n; i++) {
        //    cout << (l + diff) % (n-1) << endl;
        //}

        // Flaw: if n is power of 2, then n-1 being odd, will never divide it, making complexity O(t * k)
        int modder = n % 2 ? n : n-1;

        // Flaw fix
        if ((n & (n-1)) == 0) {
            int tmp = n;
            int logn = 0;
            while (tmp >>= 1) ++logn;
            k %= logn;
        }

        int diff = 1;
        int *diffs = new int[k];  // reuse old computations
        for (int i = 0; i < k; i++) {
            diffs[i] = diff;
            diff *= 2;
            diff %= modder;

            if (diff == 1) {
                k %= i+1;
                diff = diffs[k];
                break;
            }
        }
        delete [] diffs;

        //cout << "diff: " << diff << ' ' << "n-1: " << n-1 << " k: " << k << endl;
        for (int i = 0, l = 1; i < n; i++) {
            cout << l << ' ';
            l = (l + diff);
            if (l > n)
                l %= modder;
        }
        cout << endl;
    }
    return 0;
}
