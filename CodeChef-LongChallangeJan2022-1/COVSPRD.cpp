#include <iostream>
#include <cmath>

using namespace std;

int main(){
    int t;
    cin >> t;

    while (t--) {
        int n, d;
        cin >> n >> d;

        double expected = pow(2, min(10, d)) * pow(3, max(0, d-10));
        if (expected > 1e8) cout << n << endl;
        else cout << min(n, (int) expected) << endl;
    }
    return 0;
}
